
	<?php $i = 0; ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<?php $i++; ?>
		<?php $custom_fields = get_post_custom($post_id); ?>
		<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
		<div class="listFacts">
			<ul>
				<li><b>Kategorie:</b> <?php the_category(', '); ?></li>
				<li><b>Teilnehmer:</b> <?php echo checkEmpty($custom_fields['playerFrom'][0],'<i>0</i>','<i>nicht angegeben</i>'); ?> bis <?php echo checkEmpty($custom_fields['playerTo'][0],'<i>egal</i>','<i>nicht angegeben</i>'); ?></li>
				<li><b>Kommentare:</b> <?php comments_number('<i>keine</i>','%','%'); ?></li>
			</ul>
		</div>
		<div class="entry">
			<?php $ct = strip_tags(get_the_content()); if(strlen($ct) > 220 && strpos($ct, ' ', 220)) { echo substr($ct,0,strpos($ct, ' ', 220)); echo ' <span class="more">(<a href="'. get_permalink() .'">...</a>)</span>'; } else { echo $ct; } ?>
		</div>
		<?php if( in_array($i, array(1,5,10,15,20,25))) { ?>
		<div class="sda">
			<?php if ( is_active_sidebar( 'sidebar-list-'. $i ) ) {
				dynamic_sidebar( 'sidebar-list-'. $i );
			} ?>
		</div>
		<?php } ?>
	<?php endwhile; endif; ?>
