		</div>
		<div class="footer">
			<div class="changes">
				<ul>
<?php if ( is_active_sidebar( 'sidebar-footer' ) ) {
dynamic_sidebar( 'sidebar-footer' );
} ?>
				</ul>
			</div>
			<div class="finfo"><b>&copy; Copyright traudichspiele.de</b> All Rights Reserved!</div>
		</div>
		<?php wp_footer(); ?>
	</body>
</html>
