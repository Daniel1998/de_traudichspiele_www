<?php
register_sidebar( array(
'name' => 'Linke Seite',
'id' => 'sidebar-left',
'description' => '',
'before_title' => '<div class="head">',
'after_title' => '</div>',
) );
register_sidebar( array(
'name' => 'Footer Widgets',
'id' => 'sidebar-footer',
'before_title' => '<div class="head">',
'after_title' => '</div>'
) );
register_sidebar( array(
'name' => 'Content davor',
'id' => 'sidebar-content-before',
'before_widget' => '',
'after_widget' => '',
) );
register_sidebar( array(
'name' => 'Content danach',
'id' => 'sidebar-content-after',
'before_widget' => '',
'after_widget' => '',
) );
register_sidebar( array(
'name' => 'Liste 1tes Element',
'id' => 'sidebar-list-1',
'before_widget' => '',
'after_widget' => '',
) );
register_sidebar( array(
'name' => 'Liste 5tes Element',
'id' => 'sidebar-list-5',
'before_widget' => '',
'after_widget' => '',
) );
register_sidebar( array(
'name' => 'Liste 10tes Element',
'id' => 'sidebar-list-10',
'before_widget' => '',
'after_widget' => '',
) );
register_sidebar( array(
'name' => 'Liste 15tes Element',
'id' => 'sidebar-list-15',
'before_widget' => '',
'after_widget' => '',
) );
register_sidebar( array(
'name' => 'Liste 20tes Element',
'id' => 'sidebar-list-20',
'before_widget' => '',
'after_widget' => '',
) );
register_sidebar( array(
'name' => 'Liste 25tes Element',
'id' => 'sidebar-list-25',
'before_widget' => '',
'after_widget' => '',
) );

add_theme_support('post-thumbnails');

if ( ! current_user_can( 'manage_options' ) ) {
    show_admin_bar( false );
}

function fb_disable_feed() {
	wp_die( __('Der Feed wurde deaktiviert. Besuchen Sie bitte die <a href="'. get_bloginfo('url') .'">Startseite</a>, um die neusten Inhalte zu sehen!') );
}
 
add_action('do_feed', 'fb_disable_feed', 1);
add_action('do_feed_rdf', 'fb_disable_feed', 1);
add_action('do_feed_rss', 'fb_disable_feed', 1);
add_action('do_feed_rss2', 'fb_disable_feed', 1);
add_action('do_feed_atom', 'fb_disable_feed', 1);

function addHomeMenuLink($menuItems, $args)
{
	if ( is_front_page() )
		$class = 'class="current_page_item"';
	else
		$class = '';

	$homeMenuItem = '<li ' . $class . '>' .
					$args->before .
					'<a href="' . home_url( '/' ) . '" title="Home">' .
						$args->link_before .
						'<img src="'. get_bloginfo('template_directory') .'/house.png" alte="H" />' .
						$args->link_after .
					'</a>' .
					$args->after .
					'</li>';

	$menuItems = $homeMenuItem . $menuItems;


	return $menuItems;
}

add_filter( 'wp_nav_menu_items', 'addHomeMenuLink', 10, 2 );


function checkEmpty($str, $emptyStr, $nullStr = '') {
	if (!isset($str) || $str == 'NULL') {
		$return = $nullStr;
	} elseif(empty($str)){
		$return = $emptyStr;
	} else {
		$return = $str;
	}
	return $return;
}

    // Custom callback to list comments in the your-theme style
    function custom_comments($comment, $args, $depth) {
      $GLOBALS['comment'] = $comment;
     $GLOBALS['comment_depth'] = $depth;
      ?>
       <li id="comment-<?php comment_ID() ?>" <?php comment_class() ?>>
        <div class="comment-author vcard"><?php commenter_link() ?></div>
        <div class="comment-meta"><?php printf(__('%1$s at %2$s <span class="meta-sep">|</span> <a href="%3$s" title="Permalink to this comment">Permalink</a>', 'your-theme'),
           get_comment_date(),
           get_comment_time(),
           '#comment-' . get_comment_ID() );
           edit_comment_link(__('Edit', 'your-theme'), ' <span class="meta-sep">|</span> <span class="edit-link">', '</span>'); ?></div>
      <?php if ($comment->comment_approved == '0') _e("\t\t\t\t\t<span class='unapproved'>Your comment is awaiting moderation.</span>\n", 'your-theme') ?>
              <div class="comment-content">
            <?php comment_text() ?>
        </div>
      <?php // echo the comment reply link
       if($args['type'] == 'all' || get_comment_type() == 'comment') :
        comment_reply_link(array_merge($args, array(
         'reply_text' => __('Reply','your-theme'),
         'login_text' => __('Log in to reply.','your-theme'),
         'depth' => $depth,
         'before' => '<div class="comment-reply-link">',
         'after' => '</div>'
        )));
       endif;
      ?>
    <?php } // end custom_comments

    // Custom callback to list pings
    function custom_pings($comment, $args, $depth) {
           $GLOBALS['comment'] = $comment;
            ?>
          <li id="comment-<?php comment_ID() ?>" <?php comment_class() ?>>
           <div class="comment-author"><?php printf(__('By %1$s on %2$s at %3$s', 'your-theme'),
             get_comment_author_link(),
             get_comment_date(),
             get_comment_time() );
             edit_comment_link(__('Edit', 'your-theme'), ' <span class="meta-sep">|</span> <span class="edit-link">', '</span>'); ?></div>
        <?php if ($comment->comment_approved == '0') _e('\t\t\t\t\t<span class="unapproved">Your trackback is awaiting moderation.</span>\n', 'your-theme') ?>
                <div class="comment-content">
           <?php comment_text() ?>
       </div>
    <?php } // end custom_pings

    // Produces an avatar image with the hCard-compliant photo class
    function commenter_link() {
     $commenter = get_comment_author_link();
     if ( ereg( '<a[^>]* class=[^>]+>', $commenter ) ) {
      $commenter = ereg_replace( '(<a[^>]* class=[\'"]?)', '\\1url ' , $commenter );
     } else {
      $commenter = ereg_replace( '(<a )/', '\\1class="url "' , $commenter );
     }
     $avatar_email = get_comment_author_email();
     $avatar = str_replace( "class='avatar", "class='photo avatar", get_avatar( $avatar_email, 50 ) );
     echo $avatar . ' <span class="fn n">' . $commenter . '</span>';
    } // end commenter_link


?>
