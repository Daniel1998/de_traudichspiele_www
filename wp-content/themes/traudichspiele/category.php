<?php  get_header(); ?>
<?php  get_sidebar(); ?>
<div class="content box">
	<?php if(is_category()) { ?>
		<h1><a href="<?php echo get_category_link($cat);?>"><?php single_cat_title(''); ?></a></h1>
		<div class="catinfo">
			<b>Beschreibung:</b> <?php echo checkEmpty(category_description(), '<i>keine</i>'); ?>
		</div>
	<?php } ?>

	<?php get_template_part( 'loop', 'category' ); ?>

</div>
<?php get_footer(); ?>