<?php  get_header(); ?>
<?php  get_sidebar(); ?>
<div class="content box">
	<?php $user = get_userdata( $author ); ?>
	<h1><a href="<?php echo get_author_posts_url( $author ); ?>"><?php echo $user->user_nicename ?>'s Spiele</a></h1>
	<div class="catinfo"></div>

	<?php get_template_part( 'loop', 'category' ); ?>

</div>
<?php get_footer(); ?>