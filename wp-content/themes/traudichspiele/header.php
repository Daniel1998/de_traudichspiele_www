<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
	<head>
		<title><?php wp_title('-',true,'right'); ?><?php bloginfo('name'); ?> - <?php bloginfo('description') ?> - Komunikationsspiele, Kennenlernspiele, Refelxionsmethoden, Gruppenspiele, Seminare</title>
		<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
		<meta http-equiv="content-type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<meta name="author" content="Benedikt Pauli" />
		<meta name="description" content="<?php bloginfo('description'); ?>" />
		<meta name="robots" content="follow" />
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<?php wp_head(); ?> 
	</head>
	<body>
		<div class="user">
			<?php
				global $user_ID, $user_identity;
				get_currentuserinfo();
				if (!$user_ID):
			?>
			<form action="<?php echo get_settings('siteurl'); ?>/wp-login.php" method="post">
				<input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>"/>
	            <input type="text" name="log" class="user" value="Benutzername" onblur="if (this.value == '') {this.value='Benutzername'; this.style.color='#ccc';}" onfocus="if (this.value == 'Benutzername') {this.value=''; this.style.color='#000';}" />
	            <input type="password" name="pwd" class="password" value="Passwort" onblur="if (this.value == '') {this.value='Passwort'; this.style.color='#ccc';}" onfocus="if (this.value == 'Passwort') {this.value=''; this.style.color='#000';}" />
	            <input type="submit" name="submit" value="einloggen" class="loginbutton"/> |
	            <a href="<?php bloginfo('url'); ?>/wp-login.php?action=lostpassword">Passwort vergessen</a> |
				<a href="">registrieren</a>
			</form>
			<?php
				else:
					global $current_user;
	      			get_currentuserinfo();
			?>
				<span>Angemeldet als:</span> <a href=""><?php echo get_avatar($current_user->ID); ?></a> <?php /* <a href=""> */ ?><b><?php echo $current_user->display_name; ?></b><?php /* </a> */ ?> |
				<a href="<?php echo get_option('siteurl'); ?>/author/<?php echo $current_user->user_nicename; ?>">meine Spiele</a> |
				<?php if ($current_user->user_level >= '10') { ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/">Admin-CP</a> | <?php }?>
				<a href="<?php echo wp_logout_url( $_SERVER['REQUEST_URI'] ); ?>"><button class="loginbutton">ausloggen</button></a>
			<?php endif; ?>
		</div>
		<!-- <div class="user">
			<form><input type="text" /><input type="password" /> <input type="submit" value="login" /></form> | <a href="">registrieren</a>
		</div> -->
		<div class="header cb">
			<div class="sda">
				<script type="text/javascript">
					adscale_slot_id="MzI5MmMw";
				</script>
				<script type="text/javascript" src="//js.adscale.de/getads.js"></script>
			</div>
			<div class="title">
				<h1>trau<i>dich</i>spiele<span class="tld">.de</span></h1>
				<h2>Spielesammlung f&uuml;r jede Gelegenheit</h2>
			</div>
		</div>
		<div class="navigation">
			<div class="navline">
				<ul>
					<?php wp_nav_menu( array( 'container' => '', 'fallback_cb' => '' ) ); ?>

					<li class="right search">
							<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get"><input type="text" value="<?php echo checkEmpty(wp_specialchars($s, 1), 'Neue Suche') ?>" name="s" onblur="if (this.value == '') {this.value='Neue Suche'; this.style.color='#ccc';}" onfocus="if (this.value == 'Neue Suche') {this.value=''; this.style.color='#000';}" /></form>
					</li>
				</ul>
			</div>
		</div>

		<div class="main">
