<?php /* Template Name: Kategorien Template */ ?>
<?php get_header(); ?>
<?php  get_sidebar(); ?>
<div class="content box">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<h1><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
		<p><?php the_content(); ?></p>
		<?php
		$categories = get_categories(array('orderby' => 'name','order' => 'ASC','hide_empty' => 0));
			foreach($categories as $category) { 
				echo '<h2><a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> <span>('. $category->count . ($category->count == 1 ? ' Spiel' : ' Spiele') .')</span></h2> ';
				echo '<p>';
					$ct = strip_tags(category_description( $category->term_id ));
					if(strlen($ct) > 230 && strpos($ct, ' ', 230)) {
						echo substr($ct,0,strpos($ct, ' ', 230));
						echo ' <span class="more">(<a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>...</a>)</span>';
					} else {
						echo $ct;
					}
				echo '</p>';
			} 
		?>

	<?php endwhile; endif; ?>

</div>
<?php get_footer(); ?>