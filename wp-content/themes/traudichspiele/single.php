<?php get_header(); ?>
<?php  get_sidebar(); ?>
<div class="content box">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<h1><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
		<ul class="facts">
			<li>
				<span class="title">Kategorie</span>
				<span class="fact"><?php the_category(', '); ?></span>
			</li>
			<li>
				<span class="title">Eingef&uuml;gt</span>
				<span class="fact"><a href=""><?php echo get_avatar( get_the_author_meta('ID') ); ?></a> <a href=""><b><?php the_author(); ?></b></a> am <?php the_date(); ?></span>
			</li>
			<?php
			$custom_fields = get_post_custom($post_id);
			?>
			<?php /*
			<li>
				<span class="title">Bewertung</span>
				<span class="fact"><a class="stars" href=""><img src="<?php bloginfo('template_directory') ?>/star.png" alt="*" /><img src="<?php bloginfo('template_directory') ?>/star.png" alt="*" /><img src="<?php bloginfo('template_directory') ?>/star.png" alt="*" /><img src="<?php bloginfo('template_directory') ?>/star.png" alt="*" /><img src="<?php bloginfo('template_directory') ?>/star_gray.png" alt="*" /></a></span>
			</li>
			*/ ?>
			<li>
				<span class="title">Spieler</span>
				<span class="fact"><?php echo checkEmpty($custom_fields['playerFrom'][0],'<i>0</i>','<i>nicht angegeben</i>'); ?> bis <?php echo checkEmpty($custom_fields['playerTo'][0],'<i>egal</i>','<i>nicht angegeben</i>'); ?></span>
			</li>
			<li>
				<span class="title">Alter</span>
				<span class="fact"><?php echo checkEmpty($custom_fields['ageFrom'][0],'<i>0</i>','<i>nicht angegeben</i>'); ?> bis <?php echo checkEmpty($custom_fields['ageTo'][0],'<i>egal</i>','<i>nicht angegeben</i>'); ?></span>
			</li>
			<li>
				<span class="title">Dauer</span>
				<span class="fact"><?php echo checkEmpty($custom_fields['duration'][0],'<i>0</i>','<i>nicht angegeben</i>'); ?> Minuten</span>
			</li>
			<li>
				<span class="title">Ort</span>
				<span class="fact"><?php echo checkEmpty($custom_fields['place'][0],'<i>egal</i>','<i>nicht angegeben</i>'); ?></span>
			</li>
			<li>
				<span class="title">Material</span>
				<span class="fact"><?php echo checkEmpty($custom_fields['material'][0],'<i>nichts</i>','<i>nicht angegeben</i>'); ?></span>
			</li>
		</ul>

		<div class="sda">
<?php if ( is_active_sidebar( 'sidebar-content-before' ) ) {
dynamic_sidebar( 'sidebar-content-before' );
} ?>
		</div>

		<h5>Beschreibung:</h5>
		<?php the_content(); ?>

                <div class="sda">
<?php if ( is_active_sidebar( 'sidebar-content-after' ) ) {
dynamic_sidebar( 'sidebar-content-after' );
} ?>
                </div>


	<?php comments_template( '', true ); ?>
	<?php endwhile; endif; ?>

</div>
<?php get_footer(); ?>
