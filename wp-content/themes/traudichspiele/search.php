<?php  get_header(); ?>
<?php  get_sidebar(); ?>
<div class="content box">
	<h1><a>Deine Suche nach "<?php the_search_query(); ?>"</a></h1>
	<div class="catinfo"></div>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<?php $custom_fields = get_post_custom($post_id); ?>
		<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
		<div class="listFacts">
			<ul>
				<li><b>Kategorie:</b> <a href=""><?php the_category(', '); ?></a></li>
				<li><b>Teilnehmer:</b> <?php echo checkEmpty($custom_fields['playerFrom'][0],'<i>0</i>','<i>nicht angegeben</i>'); ?> bis <?php echo checkEmpty($custom_fields['playerTo'][0],'<i>egal</i>','<i>nicht angegeben</i>'); ?></li>
				<li><b>Kommentare:</b> <?php comments_number('<i>keine</i>','%','%'); ?></li>
			</ul>
		</div>
		<div class="entry">
			<?php $ct = strip_tags(get_the_content()); echo substr($ct,0,strpos($ct, ' ', 180)); if(strlen($ct) > 180) echo ' <span class="more">(<a href="'. get_permalink() .'">...</a>)</span>'; ?>
		</div>
	<?php endwhile; endif; ?>

</div>
<?php get_footer(); ?>