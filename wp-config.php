<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'de_traudichspiele_www');

/** MySQL database username */
define('DB_USER', 'de_traudichspiel');

/** MySQL database password */
define('DB_PASSWORD', 'aucuraq0t9la');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0;kDhD@H2k)]|d}%Re+I!T7@WNiS.egMSgV} LLP!9kzP/!zj}yd<U!YyY-Y>qvx');
define('SECURE_AUTH_KEY',  '3x;5u{[(#RG1;Pv.u>>`R{QS<IDeGM})bMn4P-e9O)Lj8(!CBVY0umf_u7{6O[!t');
define('LOGGED_IN_KEY',    'l}HLze>SX`cO;SdM)G~di <7]}926xO>q3,2dMJzvM53iSTrM$J8Z!2yjm-cP{ST');
define('NONCE_KEY',        'u]m]a3S_UB>ky E,J#Wna hh&;k1TTEne)ztwXeUr6lo;(msROAiwjTOL1Zl*L5;');
define('AUTH_SALT',        'ig&(@l<MmT8D>L$ZPyFqLQR.N!8Q~RH=&fO5I%-,m[@k=z-^NLhd<|%41WE|4vL0');
define('SECURE_AUTH_SALT', 'kC9H62^JPVT.a;]U,u5Q@xOyT6q5lKQC/fXyP8OAO4Zb7)`yBjd?*Hp,az-(gKjk');
define('LOGGED_IN_SALT',   'B^Ov$U=I3m*z`3?2T3DH]z|ne#D 5::h=IvP}neU~d9,Hq`UG(zFTu}_nuo+~C{L');
define('NONCE_SALT',       'iYLI#-nz[5QSX=Or!{UlxrUm0K%W^HYmG>/WJnU:now]tZ3//h#,UrS~qp|u|]16');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define ('WPLANG', 'de_DE');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
